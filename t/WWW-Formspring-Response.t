use Test::More tests => 10;
BEGIN { use_ok('WWW::Formspring::Response') };
use_ok( 'WWW::Formspring::User' );

my $res = WWW::Formspring::Response->new( id => 633709629,
                                      question => 'Testing question.',
                                      answer => 'Testing response.',
                                      time => 'TIME',);

my $user = WWW::Formspring::User->new(username => 'worr2400',
                                      name => 'Will');

isa_ok( $res, 'WWW::Formspring::Response' );
is( $res->id, 633709629,                        'get res_id' );
is( $res->question, 'Testing question.',        'get question' );
is( $res->answer, 'Testing response.',          'get answer'  );
is( $res->time,'TIME',                          'get time' );
is( $res->has_asked_by, '',                     'has_asked_by with undef asked_by' );

$res->asked_by($user);

isa_ok( $res->asked_by, 'WWW::Formspring::User' );
is( $res->has_asked_by, '1',                    'has_asked_by with asked_by defined' );
